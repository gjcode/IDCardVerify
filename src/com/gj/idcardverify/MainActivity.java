package com.gj.idcardverify;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {

	private EditText txt_idcardnum = null;
	private Context context = null;

	private void showToast(String text) {
		Toast toast = Toast.makeText(context, text, Toast.LENGTH_LONG);

		toast.show();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		context = this;

		Button btn_verify = (Button) findViewById(R.id.btn_verify);
		txt_idcardnum = (EditText) findViewById(R.id.textView1);

		btn_verify.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				String idCardNum = txt_idcardnum.getText() + "";

				boolean result = IDCardValidator.isValidatedIdcard(idCardNum);

				String showText = "身份证号码:" + idCardNum + "  状态:"
						+ (result ? "有效" : "无效");

				showToast(showText);
			}
		});
	}

}
